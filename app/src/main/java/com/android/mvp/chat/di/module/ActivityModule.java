/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.android.mvp.chat.di.module;

import android.content.Context;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.mvp.chat.di.ActivityContext;
import com.android.mvp.chat.di.PerActivity;
import com.android.mvp.chat.ui.home.HomeMvpPresenter;
import com.android.mvp.chat.ui.home.HomeMvpView;
import com.android.mvp.chat.ui.home.HomePresenter;
import com.android.mvp.chat.data.network.model.BlogResponse;
import com.android.mvp.chat.data.network.model.OpenSourceResponse;
import com.android.mvp.chat.ui.about.AboutMvpPresenter;
import com.android.mvp.chat.ui.about.AboutMvpView;
import com.android.mvp.chat.ui.about.AboutPresenter;
import com.android.mvp.chat.ui.feed.FeedMvpPresenter;
import com.android.mvp.chat.ui.feed.FeedMvpView;
import com.android.mvp.chat.ui.feed.FeedPagerAdapter;
import com.android.mvp.chat.ui.feed.FeedPresenter;
import com.android.mvp.chat.ui.feed.blogs.BlogAdapter;
import com.android.mvp.chat.ui.feed.blogs.BlogMvpPresenter;
import com.android.mvp.chat.ui.feed.blogs.BlogMvpView;
import com.android.mvp.chat.ui.feed.blogs.BlogPresenter;
import com.android.mvp.chat.ui.feed.opensource.OpenSourceAdapter;
import com.android.mvp.chat.ui.feed.opensource.OpenSourceMvpPresenter;
import com.android.mvp.chat.ui.feed.opensource.OpenSourceMvpView;
import com.android.mvp.chat.ui.feed.opensource.OpenSourcePresenter;
import com.android.mvp.chat.ui.login.LoginMvpPresenter;
import com.android.mvp.chat.ui.login.LoginMvpView;
import com.android.mvp.chat.ui.login.LoginPresenter;
import com.android.mvp.chat.ui.main.MainMvpPresenter;
import com.android.mvp.chat.ui.main.MainMvpView;
import com.android.mvp.chat.ui.main.MainPresenter;
import com.android.mvp.chat.ui.splash.SplashMvpPresenter;
import com.android.mvp.chat.ui.splash.SplashMvpView;
import com.android.mvp.chat.ui.splash.SplashPresenter;
import com.android.mvp.chat.utils.rx.AppSchedulerProvider;
import com.android.mvp.chat.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by janisharali on 27/01/17.
 */

@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    @PerActivity
    MainMvpPresenter<MainMvpView> provideMainPresenter(
            MainPresenter<MainMvpView> presenter) {
        return presenter;
    }

    @Provides
    SplashMvpPresenter<SplashMvpView> provideSplashPresenter(
            SplashPresenter<SplashMvpView> presenter) {
        return presenter;
    }

    @Provides
    LoginMvpPresenter<LoginMvpView> provideLoginPresenter(
            LoginPresenter<LoginMvpView> presenter) {
        return presenter;
    }

    @Provides
    HomeMvpPresenter<HomeMvpView> provideHomeMvpPresenter(
            HomePresenter<HomeMvpView> presenter) {
        return presenter;
    }

    @Provides
    AboutMvpPresenter<AboutMvpView> provideAboutPresenter(
            AboutPresenter<AboutMvpView> presenter) {
        return presenter;
    }

    @Provides
    FeedMvpPresenter<FeedMvpView> provideFeedPresenter(
            FeedPresenter<FeedMvpView> presenter) {
        return presenter;
    }

    @Provides
    OpenSourceMvpPresenter<OpenSourceMvpView> provideOpenSourcePresenter(
            OpenSourcePresenter<OpenSourceMvpView> presenter) {
        return presenter;
    }

    @Provides
    BlogMvpPresenter<BlogMvpView> provideBlogMvpPresenter(
            BlogPresenter<BlogMvpView> presenter) {
        return presenter;
    }

    @Provides
    FeedPagerAdapter provideFeedPagerAdapter(AppCompatActivity activity) {
        return new FeedPagerAdapter(activity.getSupportFragmentManager());
    }

    @Provides
    OpenSourceAdapter provideOpenSourceAdapter() {
        return new OpenSourceAdapter(new ArrayList<OpenSourceResponse.Repo>());
    }

    @Provides
    BlogAdapter provideBlogAdapter() {
        return new BlogAdapter(new ArrayList<BlogResponse.Blog>());
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(AppCompatActivity activity) {
        return new LinearLayoutManager(activity);
    }
}
