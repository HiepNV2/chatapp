package com.android.mvp.chat.ui.home;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.mvp.chat.R;
import com.android.mvp.chat.di.component.ActivityComponent;
import com.android.mvp.chat.ui.base.BaseFragment;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class HomeFragment extends BaseFragment implements HomeMvpView {

    public static final String TAG = HomeFragment.class.getSimpleName();

    @Inject
    HomeMvpPresenter<HomeMvpView> mPresenter;

    public static HomeFragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {

    }
}
