package com.android.mvp.chat.ui.login;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.mvp.chat.R;
import com.android.mvp.chat.di.component.ActivityComponent;
import com.android.mvp.chat.ui.base.BaseFragment;
import com.android.mvp.chat.ui.home.HomeFragment;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginFragment extends BaseFragment implements LoginMvpView {

    public static final String TAG = LoginFragment.class.getSimpleName();

    @Inject
    LoginMvpPresenter<LoginMvpView> mPresenter;

    public static LoginFragment newInstance() {
        Bundle args = new Bundle();
        LoginFragment fragment = new LoginFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {

    }

    @OnClick(R.id.btn_server_login)
    public void onLogin() {
        mPresenter.onLogin();
    }

    @Override
    public void openHomeFragment() {
        if (getActivity() != null && getActivity().getSupportFragmentManager() != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                    .replace(R.id.frameContainer, HomeFragment.newInstance(), HomeFragment.TAG)
                    .commit();
        }
    }
}
